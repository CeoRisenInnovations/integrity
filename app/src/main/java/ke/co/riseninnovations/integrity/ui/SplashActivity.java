package ke.co.riseninnovations.integrity.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import java.util.concurrent.TimeUnit;
import ke.co.riseninnovations.integrity.ui.login.EmailPasswordActivity;

public class SplashActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));
    Intent intent = new Intent(this, EmailPasswordActivity.class);
    startActivity(intent);
    finish();
  }
}