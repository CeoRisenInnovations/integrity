package ke.co.riseninnovations.integrity.ui.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import ke.co.riseninnovations.integrity.ui.MainActivity;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.ResetPasswordActivity;
import ke.co.riseninnovations.integrity.model.User;
import ke.co.riseninnovations.integrity.ui.base.BaseActivity;

/**
 * Created by simon on 8/23/2016.
 */
public class SignupActivity extends BaseActivity {
  private static final String TAG = "SignUpActivity";
  @BindView(R.id.email) EditText mEmailField;
  @BindView(R.id.password) EditText mPasswordField;
  @BindView(R.id.username) EditText mUsername;
  private Button btnSignIn, btnSignUp, btnResetPassword;
  private ProgressBar progressBar;
  private FirebaseAuth auth;
  private DatabaseReference database;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_signup);
    ButterKnife.bind(this);

    //Get Firebase auth instance
    database = FirebaseDatabase.getInstance().getReference();
    auth = FirebaseAuth.getInstance();

    progressBar = (ProgressBar) findViewById(R.id.progressBar);
    btnResetPassword = (Button) findViewById(R.id.btn_reset_password);

    btnResetPassword.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        startActivity(new Intent(SignupActivity.this, ResetPasswordActivity.class));
      }
    });


  }

  @Override protected void onResume() {
    super.onResume();
    progressBar.setVisibility(View.GONE);
  }

  @OnClick(R.id.sign_in_button) public void alreadyRegistered() {
    finish();
  }

  @OnClick(R.id.sign_up_button) public void createAccount() {
    createAccount(mEmailField.getText().toString().trim(), mPasswordField.getText().toString(),
        mUsername.getText().toString().trim());
  }

  private void createAccount(final String email, final String password, final String username) {
    Log.d(TAG, "createAccount: " + email);
    if (!validateForm()) {
      return;
    }
    showProgressDialog();

    database.child("taken-usernames").addListenerForSingleValueEvent(new ValueEventListener() {
      @Override public void onDataChange(DataSnapshot snapshot) {
        if (snapshot.hasChild(username)) {
          Log.v(TAG, "User name is there");
          Toast.makeText(SignupActivity.this,
              "Username is taken please choose a different one or variation of the same",
              Toast.LENGTH_LONG).show();
          hideProgressDialog();
        } else {
          // [START create_user_with_email]
          auth.createUserWithEmailAndPassword(email, password)
              .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                @Override public void onComplete(@NonNull Task<AuthResult> task) {
                  Toast.makeText(SignupActivity.this,
                      "createUserWithEmail: onComplete: " + task.isSuccessful(), Toast.LENGTH_SHORT)
                      .show();

                  // If sign in fails, display a message to the user. If sign in succeeds
                  // the auth state listener will be notified and logic to handle the
                  // signed in user can be handled in the listener.
                  if (!task.isSuccessful()) {
                    Toast.makeText(SignupActivity.this,
                        "Authentication failed. " + task.getException(), Toast.LENGTH_SHORT).show();
                  } else {
                    onAuthSuccess(task.getResult().getUser());
                  }
                }
              });
          // [END create_user_with_email]
          Log.v(TAG, "User name does not exist, create account");
        }
      }

      @Override public void onCancelled(DatabaseError databaseError) {

      }
    });
  }

  private void onAuthSuccess(final FirebaseUser user) {
    final String username = mUsername.getText().toString().trim();

    writeNewUser(user.getUid(), username, user.getEmail());

    // Go to MainActivity
    startActivity(new Intent(SignupActivity.this, MainActivity.class));
    finish();
  }

  private void writeNewUser(String userId, String name, String email) {
    Log.v(TAG, "Write New User");
    User user = new User(name, email);

    database.child("users").child(userId).setValue(user);
    database.child("taken-usernames/" + name.toLowerCase()).setValue(userId);
  }

  private boolean validateForm() {
    boolean valid = true;

    String email = mEmailField.getText().toString();
    if (TextUtils.isEmpty(email)) {
      mEmailField.setError("Required.");
      valid = false;
    } else {
      mEmailField.setError(null);
    }

    String password = mPasswordField.getText().toString();
    if (TextUtils.isEmpty(password)) {
      mPasswordField.setError("Required.");
      valid = false;
    } else {
      mPasswordField.setError(null);
    }

    return valid;
  }
}