package ke.co.riseninnovations.integrity.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties public class PostDetail {

  public String postTitle;
  public String postImage;
  public String postBody;
  public String upvotes;
  public String uid;
  public String timeCreated;
  public String category;
  public String pushId;
  public String commentBody;
  public String userName;

  public PostDetail() {
    // Empty Method needed for Firebase functionality
  }

  public PostDetail(String upvotes, String postTitle, String postImage, String postBody,
      String commentBody, String uid, String pushId, String userName, String timeCreated) {
    this.upvotes = upvotes;
    this.postTitle = postTitle;
    this.postImage = postImage;
    this.postBody = postBody;
    this.commentBody = commentBody;
    this.uid = uid;
    this.pushId = pushId;
    this.userName = userName;
    this.timeCreated = timeCreated;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public void setUpvotes(String upvotes) {
    this.upvotes = upvotes;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public void setTimeCreated(String timeCreated) {
    this.timeCreated = timeCreated;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public void setPushId(String pushId) {
    this.pushId = pushId;
  }

  public String getPostTitle() {
    return postTitle;
  }

  public void setPostTitle(String postTitle) {
    this.postTitle = postTitle;
  }

  public String getPostImage() {
    return postImage;
  }

  public void setPostImage(String postImage) {
    this.postImage = postImage;
  }

  public String getPostBody() {
    return postBody;
  }

  public void setPostBody(String postBody) {
    this.postBody = postBody;
  }

  public String getCommentBody() {
    return commentBody;
  }

  public void setCommentBody(String commentBody) {
    this.commentBody = commentBody;
  }

  @Exclude public Map<String, Object> toMap() {
    HashMap<String, Object> result = new HashMap<>();
    result.put("upvotes", upvotes);
    result.put("postBody", postBody);
    result.put("commentBody", commentBody);
    result.put("uid", uid);
    result.put("pushId", pushId);
    result.put("userName", userName);
    result.put("timeCreated", timeCreated);
    return result;
  }
}
