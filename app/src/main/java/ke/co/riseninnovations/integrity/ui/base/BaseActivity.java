package ke.co.riseninnovations.integrity.ui.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {
  public static final String BASETAG = "BaseActivity";
  @VisibleForTesting public ProgressDialog mProgressDialog;

  protected FragmentManager fm = getSupportFragmentManager();
  protected FragmentTransaction ft = fm.beginTransaction();

  public void setActionBarTitle(String title) {
    getSupportActionBar().setTitle(title);
  }

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override protected void onResume() {
    super.onResume();
  }

  @Override protected void onDestroy() {
    fm = null;
    super.onDestroy();
  }


  public void showProgressDialog() {
    if (mProgressDialog == null) {
      mProgressDialog = new ProgressDialog(this);
      mProgressDialog.setIndeterminate(true);
    }

    mProgressDialog.show();
  }

  public void hideProgressDialog() {
    if (mProgressDialog != null && mProgressDialog.isShowing()) {
      mProgressDialog.dismiss();
    }
  }

  @Override public void onStop() {
    super.onStop();
    hideProgressDialog();
  }
}
