package ke.co.riseninnovations.integrity.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class NewsFeed {

  public String category;
  public String upvotes;
  public String uid;
  public String postTitle;
  public String timeCreated;
  public String userName;
  public String body;
  public String postImage;
  public String pushId;

  public NewsFeed() {
    // Empty Method needed for Firebase functionality
  }

  public NewsFeed(String postTitle, String category, String pushId, String timeCreated, String uid,
      String upvotes, String userName) {
    this.category = category;
    this.pushId = pushId;
    this.timeCreated = timeCreated;
    this.uid = uid;
    this.upvotes = upvotes;
    this.userName = userName;
    this.postTitle = postTitle;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getUid() {
    return uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String getUpvotes() {
    return upvotes;
  }

  public void setUpvotes(String upvotes) {
    this.upvotes = upvotes;
  }

  public String getTimeCreated() {
    return timeCreated;
  }

  public void setTimeCreated(String timeCreated) {
    this.timeCreated = timeCreated;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getPushId() {
    return pushId;
  }

  public void setPushId(String pushId) {
    this.pushId = pushId;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getPostImage() {
    return postImage;
  }

  public void setPostImage(String postImage) {
    this.postImage = postImage;
  }

  public String getPostTitle() {
    return postTitle;
  }

  public void setPostTitle(String postTitle) {
    this.postTitle = postTitle;
  }

  @Exclude public Map<String, Object> toMap() {
    HashMap<String, Object> result = new HashMap<>();
    result.put("timeCreated", timeCreated);
    result.put("uid", uid);
    result.put("category", category);
    result.put("userName", userName);
    result.put("body", body);
    result.put("postImage", postImage);
    result.put("pushId", pushId);
    result.put("upvotes", upvotes);
    result.put("postTitle", postTitle);
    return result;
  }
}
