package ke.co.riseninnovations.integrity.ui.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.widget.Toast;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import ke.co.riseninnovations.integrity.AddFragmentHandler;

public abstract class BaseFragment extends Fragment {
  public OnFragmentInteractionListener mFragListener;
  @VisibleForTesting public ProgressDialog mProgressDialog;
  protected IFirebaseReferenceListener mListener;
  protected ICandidateProfileListener mCandidateListener;
  protected DatabaseReference database = FirebaseDatabase.getInstance().getReference();
  private AddFragmentHandler fragmentHandler;

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    fragmentHandler = new AddFragmentHandler(getActivity().getSupportFragmentManager());
    super.onCreate(savedInstanceState);
  }

  public void showProgressDialog() {
    if (mProgressDialog == null) {
      mProgressDialog = new ProgressDialog(this.getActivity());
      mProgressDialog.setIndeterminate(true);
    }

    mProgressDialog.show();
  }

  public void hideProgressDialog() {
    if (mProgressDialog != null && mProgressDialog.isShowing()) {
      mProgressDialog.dismiss();
    }
  }

  public abstract String getTitle();

  public void toast(String text) {
    Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
  }

  public interface IFirebaseReferenceListener {
    void iFirebaseReferenceListener(DatabaseReference globalPostRef,
        DatabaseReference globalUserPostRef, String newsfeedKey);
  }

  public interface ICandidateProfileListener {

    void iCandidateProfileListener(DatabaseReference candidateRef);
  }

  public interface OnFragmentInteractionListener {
    void onFragmentInteraction(String title);
  }
}
