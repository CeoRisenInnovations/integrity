package ke.co.riseninnovations.integrity.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by simon on 3/25/2017.
 */
@IgnoreExtraProperties
public class Candidate {

  public String firstName;
  public String lastName;

  public Candidate() {
    // Default constructor required for calls to DataSnapshot.getValue(Candidate.class)
  }

  public Candidate(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
