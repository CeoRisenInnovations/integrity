package ke.co.riseninnovations.integrity.ui.fragments;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.database.DatabaseReference;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.ui.MainActivity;
import ke.co.riseninnovations.integrity.ui.base.BaseFragment;

public class CandidatesFragment extends BaseFragment {

  private static final String TAG = "CandidatesFragment";
  private static final String FRAG_TITLE = "Candidates";

  CandidatesFragment mCandidateFragment;
  MainActivity mMainActivity;

  @BindView(R.id.rl_candidates_gov)
  RelativeLayout mGovernor;
  @BindView(R.id.rl_candidates_sen)
  RelativeLayout mSenator;
  @BindView(R.id.rl_candidates_mp)
  RelativeLayout mMP;
  @BindView(R.id.rl_candidates_mca)
  RelativeLayout mMCA;
  @BindView(R.id.rl_candidates_county)

  RelativeLayout mCounty;
  int screenHeight;
  int screenWidth;
  double imgHeight;
  double imgWidth;
  double cHeight;

  DatabaseReference candidateRef;

  public CandidatesFragment() {
    // needed constructor for firebase
  }

  public static CandidatesFragment newInstance(String title) {
    Log.v(TAG, "new instance of " + title);
    return new CandidatesFragment();
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    Fragment candidatesFrag = CandidatesFragment.newInstance("candidatesFrag");
    candidatesFrag.setTargetFragment(this, -1);
  }

  @Override
  public void onAttach(Context context) {
    Log.v(TAG, "onAttach");
    super.onAttach(context);
    try {
      mFragListener = (OnFragmentInteractionListener) context;
    } catch (ClassCastException e) {
      Log.v(TAG, "Exception:" + e);
      throw new ClassCastException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
    if (context instanceof ICandidateProfileListener) {
      mCandidateListener = (ICandidateProfileListener) context;
    } else {
      throw new ClassCastException(context.toString() + " must implement ICandidateListener");
    }
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    Log.v(TAG, "onCreate");
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override
  public void onPrepareOptionsMenu(Menu menu) {
    menu.findItem(R.id.add_new_post).setVisible(false);
    super.onPrepareOptionsMenu(menu);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    Log.v(TAG, "onCreateView");
    setRetainInstance(true);
    if (mFragListener != null) {
      setupTitleBar();
    }
    View view = inflater.inflate(R.layout.fragment_candidates, container, false);
    ButterKnife.bind(this, view);
    calculateScreenSize();

    if (((MainActivity) getActivity()).getScreenOrientation() == ORIENTATION_PORTRAIT) {
      imgHeight = screenHeight * 0.30;
      imgWidth = screenWidth * 0.50;
    } else {
      imgHeight = screenHeight * 0.40;
      imgWidth = screenWidth * 0.25;
      mGovernor.getLayoutParams().height = (int) imgHeight;
      mSenator.getLayoutParams().height = (int) imgHeight;
      mMP.getLayoutParams().height = (int) imgHeight;
      mMCA.getLayoutParams().height = (int) imgHeight;

      cHeight = screenHeight * 0.40;
      mCounty.getLayoutParams().height = (int) cHeight;
    }

    mGovernor.getLayoutParams().width = (int) imgWidth;
    mSenator.getLayoutParams().width = (int) imgWidth;
    mMP.getLayoutParams().width = (int) imgWidth;
    mMCA.getLayoutParams().width = (int) imgWidth;

    Log.v(TAG, String.valueOf(imgHeight) + String.valueOf(imgWidth));
    return view;
  }

  private void calculateScreenSize() {
    DisplayMetrics displaymetrics = new DisplayMetrics();
    getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    screenHeight = displaymetrics.heightPixels;
    screenWidth = displaymetrics.widthPixels;
  }

  public void onActivityCreated(Bundle savedInstanceState) {
    Log.v(TAG, "onActivityCreated");
    super.onActivityCreated(savedInstanceState);
  }

  @Override
  public void onStart() {
    Log.v(TAG, "onStart");
    super.onStart();
  }

  @Override
  public void onResume() {
    Log.v(TAG, "onResume");
    mFragListener = (OnFragmentInteractionListener) getActivity();
    mFragListener.onFragmentInteraction("Candidates");
    setupTitleBar();
    super.onResume();
  }

  public void setFragmentListener(String s) {
    mFragListener = (OnFragmentInteractionListener) getActivity();
    mFragListener.onFragmentInteraction(s);
  }

  @Override
  public void setInitialSavedState(SavedState state) {
    super.setInitialSavedState(state);
  }

  @Override
  public void onPause() {
    Log.v(TAG, "onPause");
    mFragListener = null;
    super.onPause();
  }

  @Override
  public void onStop() {
    Log.v(TAG, "onStop");
    mFragListener = null;
    super.onStop();
  }

  @Override
  public void onDestroyView() {
    Log.v(TAG, "onDestroyView");
    mFragListener = null;
    super.onDestroyView();
  }

  @Override
  public void onDestroy() {
    Log.v(TAG, "onDestroy");
    super.onDestroy();
  }

  @Override
  public void onDetach() {
    Log.v(TAG, "onDetach");
    mFragListener = null;
    super.onDetach();
  }

  @Override
  public String getTitle() {
    return null;
  }

  public void setupTitleBar() {
    //mFragListener.onFragmentInteraction("Candidates");
    mMainActivity = (MainActivity) getActivity();
    mMainActivity.setTitle(FRAG_TITLE);
  }

  @OnClick(R.id.governor)
  void onGovernorClicked() {
    ((MainActivity) this.getActivity()).openGovFrag();
    mFragListener = null;
  }

  @OnClick(R.id.senator)
  void onSenatorClicked() {
    ((MainActivity) this.getActivity()).openSenatorFrag();
    mCandidateListener.iCandidateProfileListener(candidateRef);
  }

  /*@OnClick(R.id.members_parliament) void onMPClicked() {
    ((MainActivity) this.getActivity()).openMPFrag();
  }*/

  /*@OnClick(R.id.mca) void onMCAClicked() {
    ((MainActivity) this.getActivity()).openMCAFrag();
  }*/
}
