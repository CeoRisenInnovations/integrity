package ke.co.riseninnovations.integrity.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.model.NewsFeed;

public class NewsFeedHolder extends RecyclerView.ViewHolder {
    View mView;
    @BindView(R.id.thumbs_up_button)
    ImageButton upvoteView;
    @BindView(R.id.content_txt)
    TextView commentView;
    @BindView(R.id.post_title)
    TextView titleView;
    @BindView(R.id.created_by_txt) TextView usernameView;

    public NewsFeedHolder(View itemView) {
        super(itemView);
        mView = itemView;
        ButterKnife.bind(this, itemView);
    }

    public void setCategory(String category) {
        TextView field = (TextView) mView.findViewById(R.id.category_txt);
        field.setText(category);
    }

    public void setUpvotes(String upvotes) {
        TextView field = (TextView) mView.findViewById(R.id.upvote_txt);
        field.setText(upvotes);
    }

    public void setTitle(String title) {
        TextView field = (TextView) mView.findViewById(R.id.post_title);
        field.setText(title);
    }

    public void setUsername(String username) {
        TextView field = (TextView) mView.findViewById(R.id.created_by_txt);
        field.setText(username);
    }

    public void setTimeCreated(String timeCreated) {
        TextView field = (TextView) mView.findViewById(R.id.time_created);
        field.setText(timeCreated);
    }

    public void setUid(String uid) {
        TextView field = (TextView) mView.findViewById(R.id.created_by_txt);
        field.setText(uid);
    }

    public void bindToNewsFeed(NewsFeed newsFeed, View.OnClickListener newsfeedClickListener) {
        upvoteView.setOnClickListener(newsfeedClickListener);
    }

    public void commentBindToNewsFeed(NewsFeed newsFeed, View.OnClickListener newsfeedClickListener) {
        commentView.setOnClickListener(newsfeedClickListener);
    }

    public void titleBindToNewsFeed(NewsFeed newsFeed, View.OnClickListener newsFeedClickListener) {
        titleView.setOnClickListener(newsFeedClickListener);
    }
}
