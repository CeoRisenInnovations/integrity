package ke.co.riseninnovations.integrity;

import android.support.multidex.MultiDexApplication;
import com.squareup.leakcanary.LeakCanary;

/**
 * Created by simon on 11/16/2016.
 */

public class IntegrityApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
      if (BuildConfig.DEBUG) {
        if (LeakCanary.isInAnalyzerProcess(this)) {
          // This process is dedicated to LeakCanary for heap analysis.
          // You should not init your app in this process.
          return;
        }
        LeakCanary.install(this);
      }
    }
}
