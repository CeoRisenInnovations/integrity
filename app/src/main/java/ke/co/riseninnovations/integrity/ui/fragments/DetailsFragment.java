package ke.co.riseninnovations.integrity.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.model.PostDetail;
import ke.co.riseninnovations.integrity.model.PostDetailComments;
import ke.co.riseninnovations.integrity.model.User;
import ke.co.riseninnovations.integrity.ui.MainActivity;
import ke.co.riseninnovations.integrity.ui.base.BaseFragment;
import ke.co.riseninnovations.integrity.util.UpVoting;
import ke.co.riseninnovations.integrity.view.CommentsHolder;

public class DetailsFragment extends BaseFragment {
  private static final String TAG = "DetailsFragment: ";

  DatabaseReference globalPostRef;
  DatabaseReference globalUserPostRef;
  FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
  String uid = user.getUid();
  String newsfeedKey;
  String time;
  Calendar calendar;
  SimpleDateFormat simpleDateFormat;
  @BindView(R.id.topic_title_txt) TextView mPostTitle;
  @BindView(R.id.topic_body_txt) TextView mTopicBody;
  @BindView(R.id.detail_comments_view) RecyclerView commentsView;
  @BindView(R.id.comment_test_button) Button mComment;
  @BindView(R.id.progressBar) ProgressBar progressBar;
  FirebaseRecyclerAdapter<PostDetailComments, CommentsHolder> mAdapter;
  DatabaseReference commentUpVoteRef = database.child("comment-upvotes");

  // Detail Fragment Single Event Listeners
  ValueEventListener mTitleListener = new ValueEventListener() {
    @Override public void onDataChange(DataSnapshot dataSnapshot) {
      // Get Post Object and use the values to update the UI
      PostDetail postDetail = dataSnapshot.getValue(PostDetail.class);
      ((MainActivity) getActivity()).setActionBarTitle(postDetail.postTitle);
      mPostTitle.setText(postDetail.postTitle);
    }

    @Override public void onCancelled(DatabaseError databaseError) {
      Log.v(TAG, "The read failed: " + databaseError.getCode());
    }
  };

  ValueEventListener mBodyListener = new ValueEventListener() {
    @Override public void onDataChange(DataSnapshot dataSnapshot) {
      // Get Post Object and use the values to update the UI
      PostDetail postDetail = dataSnapshot.getValue(PostDetail.class);
      mTopicBody.setText(postDetail.postBody);
    }

    @Override public void onCancelled(DatabaseError databaseError) {
      Log.v(TAG, "The read failed: " + databaseError.getCode());
    }
  };

  public static DetailsFragment newInstance(String title, DatabaseReference globalPostRef,
      DatabaseReference globalUserPostRef, String newsfeedKey) {
    Log.v(TAG, "new instance of " + title + globalPostRef + globalUserPostRef + newsfeedKey);
    return new DetailsFragment();
  }

  public static boolean notEmpty(String s) {
    return s != null && (s.length() > 0);
  }

  @Override public void onAttach(Context context) {
    Log.v(TAG, "onAttach ");
    super.onAttach(context);
    if (context instanceof IFirebaseReferenceListener) {
      mListener = (IFirebaseReferenceListener) context;
    } else {
      throw new ClassCastException(
          context.toString() + " must implement IFirebaseReferenceListener.");
    }
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    Log.v(TAG, "onCreate ");
    super.onCreate(savedInstanceState);
    mListener = (IFirebaseReferenceListener) getActivity();
    setHasOptionsMenu(true);
  }

  @Override public String getTitle() {
    return null;
  }

  @Override public void onPrepareOptionsMenu(Menu menu) {
    menu.findItem(R.id.add_new_post).setVisible(false);
    super.onPrepareOptionsMenu(menu);
  }

  @Nullable @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    Log.v(TAG, "onCreateView ");
    setRetainInstance(true);
    View view = inflater.inflate(R.layout.fragment_details, container, false);
    ButterKnife.bind(this, view);
    setupRecylerview();
    showProgressDialog();
    mProgressDialog.setMessage("Loading comments...");
    listenForTitle();
    globalUserPostRef.addListenerForSingleValueEvent(mBodyListener);
    return view;
  }

  public void listenForTitle() {
    globalPostRef.addListenerForSingleValueEvent(mTitleListener);
  }

  @Override public void onActivityCreated(Bundle savedInstanceState) {
    Log.v(TAG, "onActivityCreated " + newsfeedKey);
    super.onActivityCreated(savedInstanceState);
  }

  @Override public void onStart() {
    Log.v(TAG, "onStart");
    super.onStart();
  }

  @Override public void onResume() {
    Log.v(TAG, "onResume");
    globalUserPostRef.addListenerForSingleValueEvent(mBodyListener);
    mListener = (IFirebaseReferenceListener) getActivity();
    super.onResume();
  }

  @Override public void onPause() {
    Log.v(TAG, "onPause");
    super.onPause();
    mAdapter = null;
  }

  @Override public void onStop() {
    mListener = null;
    super.onStop();
    mAdapter = null;
  }

  @Override public void onDestroyView() {
    Log.v(TAG, "onDestroyView");
    super.onDestroyView();
  }

  @Override public void onDestroy() {
    mListener = null;
    super.onDestroy();
    mAdapter = null;
  }

  @Override public void onDetach() {
    Log.v(TAG, "onDetach");
    super.onDetach();
    mAdapter = null;
  }

  public void setGlobalReferences(DatabaseReference globalPostRef,
      DatabaseReference globalUserPostRef, String newsfeedKey) {
    this.globalPostRef = globalPostRef;
    this.globalUserPostRef = globalUserPostRef;
    this.newsfeedKey = newsfeedKey;
  }

  private void setupRecylerview() {
    commentsView.setHasFixedSize(false);
    commentsView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

    mAdapter =
        new FirebaseRecyclerAdapter<PostDetailComments, CommentsHolder>(PostDetailComments.class,
            R.layout.item_comments, CommentsHolder.class,
            database.child("user-comments/" + newsfeedKey)) {
          @Override protected void onDataChanged() {
            super.onDataChanged();
            if (progressBar == null) {
              showProgressDialog();
            } else {
              hideProgressDialog();
            }
            Log.v(TAG, "DATA CHANGED");
          }

          @Override protected void populateViewHolder(final CommentsHolder commentHolder,
              final PostDetailComments postDetailCommentsMessage, final int position) {
            Log.v(TAG, "populateComments " + position);
            final DatabaseReference commentRef = getRef(position);
            final String commentKey = commentRef.getKey();
            final DatabaseReference currentCommentPath =
                commentUpVoteRef.child(newsfeedKey + "/" + commentKey);
            final DatabaseReference globalCommentRef =
                database.child("user-comments").child(newsfeedKey + "/" + commentKey);
            final DatabaseReference upVotedRef = currentCommentPath.child("/" + uid);

            // Set comment labels
            setCommentLabels(commentHolder, postDetailCommentsMessage);

            // Click listener for whole comments view
            commentHolder.itemView.setOnClickListener(new View.OnClickListener() {
              @Override public void onClick(View view) {
                Log.v(TAG, String.valueOf(position));
              }
            });

            commentHolder.commentBindToView(postDetailCommentsMessage, new View.OnClickListener() {
              Boolean EXPAND_CHECK = false;

              @Override public void onClick(View v) {
                final TextView mTextView = (TextView) v.findViewById(R.id.comment_body);

                if (!EXPAND_CHECK) {
                  mTextView.setMaxLines(5);
                  EXPAND_CHECK = true;
                } else {
                  mTextView.setMaxLines(2);
                  EXPAND_CHECK = false;
                }
              }
            });

            final ValueEventListener upvoteListener = new ValueEventListener() {
              @Override public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null) {
                  hasntUpvoted(currentCommentPath, globalCommentRef);
                } else {
                  hasUpvoted(dataSnapshot, globalCommentRef, currentCommentPath);
                }
              }

              @Override public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
              }
            };

            commentHolder.upVoteButtonBindToView(postDetailCommentsMessage,
                new View.OnClickListener() {
                  @Override public void onClick(View upvoteView) {
                    Log.v(TAG, "Listening for children on " + commentRef);

                    upVotedRef.addListenerForSingleValueEvent(upvoteListener);
                    Log.v(TAG, "Upvoted for = " + globalCommentRef);
                  }
                });
          }
        };
    Log.v(TAG, "populateCommentView");

    commentsView.setAdapter(mAdapter);
  }

  private void setCommentLabels(CommentsHolder commentHolder,
      PostDetailComments postDetailCommentsMessage) {
    commentHolder.setCommentView(postDetailCommentsMessage.getCommentBody());
    commentHolder.setUpvoteView(postDetailCommentsMessage.getUpvotes());
    commentHolder.setTimeCreatedView(postDetailCommentsMessage.getTimeCreated());
    commentHolder.setUserNameView(postDetailCommentsMessage.getUserName());
  }

  public void hasntUpvoted(DatabaseReference currentCommentPath,
      DatabaseReference globalCommentRef) {
    Log.v(TAG, "hasnt upvoted");
    currentCommentPath.child(uid).setValue(true);
    UpVoting.onCommentUpVoteClicked(globalCommentRef);
    Toast.makeText(getContext(), "Thumbs Up!", Toast.LENGTH_SHORT).show();
  }

  public void hasUpvoted(DataSnapshot dataSnapshot, DatabaseReference globalCommentRef,
      DatabaseReference currentCommentPath) {
    Log.v(TAG, "Removed Thumbs Up.");
    Boolean hasUpvoted = (Boolean) dataSnapshot.getValue();
    if (hasUpvoted) {
      UpVoting.removeCommentUpVote(globalCommentRef);
      currentCommentPath.child(uid).setValue(false);
      Toast.makeText(getContext(), "Removed Thumbs Up.", Toast.LENGTH_SHORT).show();
    } else {
      UpVoting.onCommentUpVoteClicked(globalCommentRef);
      currentCommentPath.child(uid).setValue(true);
      Toast.makeText(getContext(), "Thumbs Up!", Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.comment_test_button) void addCommentButtonClicked() {
    Log.v(TAG, "Test Comment Button Pressed");
    final EditText edittext = new EditText(getContext());
    build(edittext);
  }

  public void postComment(final String value) {
    final String pushId = database.child("post-comments/" + newsfeedKey).push().getKey();

    calendar = Calendar.getInstance();
    simpleDateFormat = new SimpleDateFormat("HH:mm ddMMyyyy", Locale.UK);

    time = simpleDateFormat.format(calendar.getTime());

    ValueEventListener usernameListener = new ValueEventListener() {
      @Override public void onDataChange(DataSnapshot dataSnapshot) {
        User mUser = dataSnapshot.getValue(User.class);
        String username = mUser.username;

        PostDetail post = new PostDetail("1", null, null, null, value, uid, pushId, username, time);
        Map<String, Object> postValues = post.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/user-comments/" + newsfeedKey + "/" + pushId, postValues);
        database.child("comment-upvotes/" + newsfeedKey + "/" + pushId + "/" + uid).setValue(true);
        database.updateChildren(childUpdates);
      }

      @Override public void onCancelled(DatabaseError databaseError) {
        Log.v(TAG, "UnableToRead: " + databaseError);
      }
    };
    database.child("users/" + uid).addListenerForSingleValueEvent(usernameListener);
  }

  public void build(final EditText s) {
    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

    builder.setMessage("Write Comment")
        .setView(s)
        .setNegativeButton("Close", null)
        .setPositiveButton("Comment", null);
    final AlertDialog alert = builder.create();
    alert.show();
    alert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {

      @Override public void onClick(View v) {
        final String value = s.getText().toString();

        // check if comment line is empty, if comment line is not empty @wantToCloseDialog = true
        // and comment is posted to the database
        if (notEmpty(value)) {
          Toast.makeText(getContext(), "Comment Sent!", Toast.LENGTH_SHORT).show();
          postComment(value);
          alert.dismiss();
          // else dialog stays open. Make sure you have an obvious way to close the dialog especially
          // if you set cancellable to false.
        } else {
          Toast.makeText(getContext(), "Must Enter Text!", Toast.LENGTH_SHORT).show();
        }
      }
    });
  }
}
