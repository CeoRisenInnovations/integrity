package ke.co.riseninnovations.integrity.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.model.NewsFeed;
import ke.co.riseninnovations.integrity.model.PostDetail;
import ke.co.riseninnovations.integrity.model.User;
import ke.co.riseninnovations.integrity.ui.base.BaseFragment;

public class NewPostFragment extends BaseFragment {
  private static final String TAG = "NewPostFragment";

  @BindView(R.id.Title) EditText postTitle;
  @BindView(R.id.body) EditText postBody;
  @BindView(R.id.post_subject) EditText postCategory;

  public NewPostFragment() {
    // needed for @firebase
  }

  public static NewPostFragment newInstance(String title) {
    Log.v(TAG, "new instance of " + title);
    return new NewPostFragment();
  }

  @Override public void onAttach(Context context) {
    Log.v(TAG, "onAttach");
    super.onAttach(context);
    // Checks if the activity implements the interface @IFirebaseReferenceListener
    if (context instanceof IFirebaseReferenceListener) {
      mListener = (IFirebaseReferenceListener) context;
    } else {
      throw new ClassCastException(
          context.toString() + " must implement IFirebaseReferenceListener");
    }
    try {
      mFragListener = (OnFragmentInteractionListener) context;
    } catch (ClassCastException e) {
      Log.v(TAG, "Exception: " + e);
      throw new ClassCastException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    Log.v(TAG, "onCreate");
    super.onCreate(savedInstanceState);
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    Log.v(TAG, "onCreateView");
    if (mFragListener != null) {
      mFragListener.onFragmentInteraction("New Post");
    }
    View view = inflater.inflate(R.layout.fragment_new_post, container, false);
    ButterKnife.bind(this, view);

    return view;
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    Log.v(TAG, "onActivityCreated");
    super.onActivityCreated(savedInstanceState);
  }

  @Override public void onStop() {
    Log.v(TAG, "onStop");
    mFragListener = null;
    super.onStop();
  }

  @Override public void onDetach() {
    Log.v(TAG, "onDetach");
    super.onDetach();
  }

  @Override public String getTitle() {
    return null;
  }

  @OnClick(R.id.button_submit) void submitPost() {
    Log.v(TAG, "Post Submitted");
    mFragListener.onFragmentInteraction("News Feed");
    // [START] Dataset objects
    DatabaseReference postUpvotedRef = database.child("post-upvotes");
    final String pushID = database.child("newsfeed").push().getKey();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    final String uid = user.getUid();

    final String time;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    // [END] Dataset objects
    TimeZone obj = TimeZone.getDefault();
    calendar = Calendar.getInstance();
    simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmm");
    simpleDateFormat.setTimeZone(obj);

    time = simpleDateFormat.format(calendar.getTime());

    toast("Post Submitted Successfully");
    if (!validateForm()) {
      return;
    }
    // [START] Receive input from text box
    final String category = postCategory.getText().toString();
    final String title = postTitle.getText().toString();
    final String body = postBody.getText().toString();
    // [END] Receive input from text box
    final DatabaseReference currentPostPath = postUpvotedRef.child(pushID);

    ValueEventListener usernameListener = new ValueEventListener() {
      @Override public void onDataChange(DataSnapshot dataSnapshot) {
        User mUser = dataSnapshot.getValue(User.class);
        String username = mUser.username;

        // [START] Data being sent to server
        NewsFeed post = new NewsFeed(title, category, pushID, time, uid, "1", username);
        PostDetail detail = new PostDetail(null, null, null, body, null, uid, pushID, null, null);

        currentPostPath.child(uid).setValue(true);
        Map<String, Object> postValues = post.toMap();
        Map<String, Object> detailValues = detail.toMap();
        Map<String, Object> postUpdates = new HashMap<>();
        Map<String, Object> detailUpdates = new HashMap<>();
        // [END] Data being sent to server

        // [START] Write to Firebase
        postUpdates.put("/posts/" + pushID, postValues);
        detailUpdates.put("/user-posts/" + pushID, detailValues);
        database.updateChildren(postUpdates);
        database.updateChildren(detailUpdates);
        // [END] Write to Firebase
        Log.v(TAG, "USERNAME IS : " + username);
      }

      @Override public void onCancelled(DatabaseError databaseError) {
        Log.v(TAG, databaseError.toString());
      }
    };
    database.child("users/" + uid).addListenerForSingleValueEvent(usernameListener);
    FragmentManager fm = getActivity().getSupportFragmentManager();
    fm.popBackStack();
  }

  private boolean validateForm() {
    boolean valid = true;
    final String req = "Required.";
    String category = postCategory.getText().toString();
    String title = postTitle.getText().toString();
    String body = postBody.getText().toString();

    if (TextUtils.isEmpty(category)) {
      postCategory.setError(req);
      valid = false;
    } else {
      postCategory.setError(null);
    }

    if (TextUtils.isEmpty(title)) {
      postTitle.setError(req);
      valid = false;
    } else {
      postTitle.setError(null);
    }

    if (TextUtils.isEmpty(body)) {
      postBody.setError(req);
      valid = false;
    } else {
      postBody.setError(null);
    }
    return valid;
  }
}
