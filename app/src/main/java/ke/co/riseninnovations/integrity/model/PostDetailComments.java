package ke.co.riseninnovations.integrity.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class PostDetailComments {

    public String category,
            upvotes,
            uid,
            commentBody,
            timeCreated,
            userName,
            pushId;

    public PostDetailComments() {
        // Empty Method needed for Firebase functionality
    }

    public PostDetailComments(String category, String upvotes, String uid, String commentBody, String timeCreated, String userName, String pushId) {
        this.category = category;
        this.upvotes = upvotes;
        this.uid = uid;
        this.commentBody = commentBody;
        this.timeCreated = timeCreated;
        this.userName = userName;
        this.pushId = pushId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(String upvotes) {
        this.upvotes = upvotes;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public void setCommentBody(String commentBody) {
        this.commentBody = commentBody;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPushId() {
        return pushId;
    }

    public void setPushId(String pushId) {
        this.pushId = pushId;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("commentBody", commentBody);
        result.put("upvotes", upvotes);
        result.put("timeCreated", timeCreated);
        result.put("userName", userName);
        return result;
    }
}
