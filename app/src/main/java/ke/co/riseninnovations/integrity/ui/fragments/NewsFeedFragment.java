package ke.co.riseninnovations.integrity.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.firebase.FirebaseRecyclerAdapter;
import ke.co.riseninnovations.integrity.model.NewsFeed;
import ke.co.riseninnovations.integrity.ui.MainActivity;
import ke.co.riseninnovations.integrity.ui.base.BaseFragment;
import ke.co.riseninnovations.integrity.util.UpVoting;
import ke.co.riseninnovations.integrity.view.NewsFeedHolder;

public class NewsFeedFragment extends BaseFragment {

  private static final String TAG = "NewsFeedFragment";
  private static final String FRAG_TITLE = "News Feed";
  NewsFeedFragment mNewsFeedFragment;
  MainActivity mMainActivity;

  @BindView(R.id.recyclerview)
  RecyclerView newsFeedView;

  FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
  String uid = user.getUid();
  DatabaseReference newsfeedRef;
  DatabaseReference postUpvotedRef = database.child("post-upvotes");
  FirebaseRecyclerAdapter<NewsFeed, NewsFeedHolder> mAdapter;

  // This listens for newsfeed values
  ValueEventListener newsFeedValueListener = new ValueEventListener() {
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
      // needed override for value event listener
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
      // Getting Post failed, log a message
      Log.w(TAG, "loadNewsFeedValues :onCancelled ", databaseError.toException());
    }
  };

  public NewsFeedFragment() {
    // Needed constructor for Firebase
  }

  public static NewsFeedFragment newInstance(String title) {
    Log.v(TAG, "new instance of " + title);
    return new NewsFeedFragment();
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    Fragment newsFeedFrag = CandidatesFragment.newInstance("newsFeedFrag");
    newsFeedFrag.setTargetFragment(this, -1);
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    // Checks if the activity implements the interface @IFirebaseReferenceListener
    if (context instanceof IFirebaseReferenceListener) {
      mListener = (IFirebaseReferenceListener) context;
    } else {
      throw new ClassCastException(
          context.toString() + " must implement IFirebaseReferenceListener");
    }
    try {
      mFragListener = (OnFragmentInteractionListener) context;
    } catch (ClassCastException e) {
      Log.v(TAG, "Exception: " + e);
      throw new ClassCastException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    setRetainInstance(true);
    if (mFragListener != null) {
      mMainActivity = (MainActivity) getActivity();
      mMainActivity.setTitle(FRAG_TITLE);
    }

    View view = inflater.inflate(R.layout.fragment_newsfeed, container, false);
    ButterKnife.bind(this, view);

    if (((MainActivity) getActivity()).isNetworkConnected()) {
      if (savedInstanceState == null) {
        setupRecyclerview();
      }
    } else {
      mProgressDialog.setMessage("No Network!");
    }

    return view;
  }

  @Override
  public void onActivityCreated(Bundle savedInstanceState) {
    Log.v(TAG, "onActivityCreated");
    super.onActivityCreated(savedInstanceState);

    mNewsFeedFragment = (NewsFeedFragment) getFragmentManager().findFragmentByTag("newsfeed");

    if (mNewsFeedFragment == null) {
      Log.v(TAG, "newsfeed frag is null");
      mNewsFeedFragment = NewsFeedFragment.newInstance("newsfeedFrag");
      mNewsFeedFragment.setTargetFragment(this, 0);

      getFragmentManager().beginTransaction().add(mNewsFeedFragment, "newsfeed").commit();
    } else {
      Log.v(TAG, "newsfeed frag is not null");
    }

  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public void onResume() {
    Log.v(TAG, "onResume");
    mListener = (IFirebaseReferenceListener) getActivity();
    mFragListener = (OnFragmentInteractionListener) getContext();
    setupTitleBar();
    setupRecyclerview();
    super.onResume();
  }

  //
  // Fragment is active
  //

  @Override
  public void onPause() {
    Log.v(TAG, "onPause");
    super.onPause();
    mListener = null;
    mAdapter = null;
    mFragListener = null;
  }

  @Override
  public void onStop() {
    super.onStop();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
  }

  @Override
  public void onDestroy() {
    Log.v(TAG, "onDestroy");
    super.onDestroy();
    mAdapter = null;
    newsFeedView = null;
    mListener = null;
    mFragListener = null;
  }

  @Override
  public void onDetach() {
    Log.v(TAG, "onDetach");
    super.onDetach();
    mFragListener = null;
    mListener = null;
  }

  public void setupTitleBar() {
    mFragListener.onFragmentInteraction("News Feed");
  }

  @OnClick(R.id.newsFeedFAB)
  public void FAB() {
    toast("FAB WORKS");
  }

  private void setupRecyclerview() {
    Log.v(TAG, "News Feed Recycler View Init...");

    showProgressDialog();
    mProgressDialog.setCancelable(false);
    mProgressDialog.setMessage("Loading News Feed...");
    LinearLayoutManager linearLayoutManager =
        new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
    linearLayoutManager.setReverseLayout(true);
    linearLayoutManager.setStackFromEnd(true);
    newsFeedView.setHasFixedSize(false);
    newsFeedView.setLayoutManager(linearLayoutManager);

    mAdapter = new FirebaseRecyclerAdapter<NewsFeed, NewsFeedHolder>(NewsFeed.class,
        R.layout.item_newsfeed, NewsFeedHolder.class,
        database.child("posts").orderByChild("timeCreated")) {

      @Override
      protected void onDataChanged() {
        super.onDataChanged();

        showProgressDialog();

        hideProgressDialog();
      }

      @Override
      public void populateViewHolder(NewsFeedHolder newsFeedHolder, final NewsFeed newsFeedMessage,
          int position) {

        newsfeedRef = getRef(position);
        final String newsfeedKey = newsfeedRef.getKey();
        final DatabaseReference globalPostRef = database.child("posts").child(newsfeedRef.getKey());
        final DatabaseReference globalUserPostRef =
            database.child("user-posts").child(newsfeedRef.getKey());
        final DatabaseReference currentPostPath = postUpvotedRef.child(newsfeedKey);
        final DatabaseReference upVotedRef = currentPostPath.child("/" + uid);

        // Set click listener for the whole post view
        newsFeedHolder.itemView.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            // Do Something Here
            globalPostRef.addListenerForSingleValueEvent(newsFeedValueListener);
          }
        });

        // [START] Set necessary newsfeed labels
        setNewsFeedHolderLabels(newsFeedHolder, newsFeedMessage);
        // [END] Get necessary newsfeed labels

        // [START] Title functionality
        newsFeedHolder.titleBindToNewsFeed(newsFeedMessage, new View.OnClickListener() {
          @Override
          public void onClick(View commentView) {
            mListener.iFirebaseReferenceListener(globalPostRef, globalUserPostRef, newsfeedKey);

            fragChange();
          }
        });
        // [END] Title functionality

        // [START] Comment button functionality
        newsFeedHolder.commentBindToNewsFeed(newsFeedMessage, new View.OnClickListener() {
          @Override
          public void onClick(View commentView) {
            onCommentClicked(globalPostRef);
          }
        });
        // [END] Comment button functionality

        final ValueEventListener upvoteListener = new ValueEventListener() {
          @Override
          public void onDataChange(DataSnapshot dataSnapshot) {
            Boolean hasUpvoted = (Boolean) dataSnapshot.getValue();
            checkHasUpvoted(dataSnapshot, currentPostPath, globalPostRef, hasUpvoted);
          }

          @Override
          public void onCancelled(DatabaseError databaseError) {
            // Getting Post failed, log a message
            Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
          }
        };

        // [START] Upvote button functionality
        newsFeedHolder.bindToNewsFeed(newsFeedMessage, new View.OnClickListener() {

          @Override
          public void onClick(View upvoteView) {
            upVotedRef.addListenerForSingleValueEvent(upvoteListener);
          }
        });
        // [END] Upvote button functionality
      }

      @Override
      protected void onCancelled(DatabaseError error) {
        toast("Network unavailable...");
        super.onCancelled(error);
      }


    };
    newsFeedView.setAdapter(mAdapter);
  }

  private void setNewsFeedHolderLabels(NewsFeedHolder newsFeedHolder, NewsFeed newsFeedMessage) {
    newsFeedHolder.setCategory(newsFeedMessage.getCategory());
    newsFeedHolder.setUpvotes(newsFeedMessage.getUpvotes());
    newsFeedHolder.setTimeCreated(newsFeedMessage.getTimeCreated());
    newsFeedHolder.setTitle(newsFeedMessage.getPostTitle());
    newsFeedHolder.setUsername(newsFeedMessage.getUserName());
  }

  public void checkHasUpvoted(DataSnapshot snapshot, DatabaseReference current,
      DatabaseReference ref, Boolean check) {
    if (snapshot.getValue() == null) {
      current.child(uid).setValue(true);
      UpVoting.onUpVoteClicked(ref);
      toast("Post liked! =]");
    } else if (check) {
      hasAlreadyUpVoted(ref, current);
    } else {
      upVotePost(ref, current);
    }
  }

  private void hasAlreadyUpVoted(DatabaseReference ref, DatabaseReference current) {
    UpVoting.removeUpVote(ref);
    current.child(uid).setValue(false);
    toast("Post Un-liked. =[");
  }

  private void upVotePost(DatabaseReference ref, DatabaseReference current) {
    UpVoting.onUpVoteClicked(ref);
    current.child(uid).setValue(true);
    toast("Post liked! =]");
  }

  public void fragChange() {
    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    if (getResources().getBoolean(R.bool.twoPaneMode)) {
      // all good, we use the fragments defined in the layout
      fragmentTransaction.hide(NewsFeedFragment.this);
      fragmentTransaction.addToBackStack("newsfeed");
      fragmentTransaction.commit();

      Log.v(TAG, "Two panel mode");
    } else {
      fragmentTransaction.hide(NewsFeedFragment.this);
      fragmentTransaction.addToBackStack("newsfeed");
      fragmentTransaction.commit();
    }
  }


  private void onCommentClicked(DatabaseReference postRef) {
    // Newsfeed comment lbl functionality
  }
}
