package ke.co.riseninnovations.integrity.util;

import android.util.Log;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import ke.co.riseninnovations.integrity.model.NewsFeed;
import ke.co.riseninnovations.integrity.model.PostDetail;

import static android.content.ContentValues.TAG;

public final class UpVoting {

  public static DatabaseReference currentCommentPath;
  public static String uid;
  public static DatabaseReference globalCommentRef;

  private UpVoting() {
    // Not Called
  }

  public static void onUpVoteClicked(DatabaseReference postRef) {
    postRef.runTransaction(new Transaction.Handler() {
      @Override public Transaction.Result doTransaction(MutableData mutableData) {
        NewsFeed n = mutableData.getValue(NewsFeed.class);
        if (n == null) {
          return Transaction.success(mutableData);
        } else {
          Integer parsedUpVotes = Integer.parseInt(n.upvotes) + 1;

          n.upvotes = parsedUpVotes.toString();
        }
        mutableData.setValue(n);
        return Transaction.success(mutableData);
      }

      @Override
      public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
        // Comment
      }
    });
  }

  public static void onCommentUpVoteClicked(DatabaseReference commentRef) {
    commentRef.runTransaction(new Transaction.Handler() {
      @Override public Transaction.Result doTransaction(MutableData mutableData) {
        PostDetail n = mutableData.getValue(PostDetail.class);
        if (n == null) {
          return Transaction.success(mutableData);
        } else {
          Integer parsedUpVotes = Integer.parseInt(n.upvotes) + 1;

          n.upvotes = parsedUpVotes.toString();
        }
        mutableData.setValue(n);
        return Transaction.success(mutableData);
      }

      @Override
      public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
        // Comment
      }
    });
  }

  public static void removeUpVote(DatabaseReference postRef) {
    postRef.runTransaction(new Transaction.Handler() {

      @Override public Transaction.Result doTransaction(MutableData mutableData) {
        NewsFeed n = mutableData.getValue(NewsFeed.class);
        if (n == null) {
          return Transaction.success(mutableData);
        } else {
          Integer parsedUpvotes = Integer.parseInt(n.upvotes) - 1;

          n.upvotes = parsedUpvotes.toString();
        }
        mutableData.setValue(n);
        return Transaction.success(mutableData);
      }

      @Override
      public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
        Log.v(TAG, String.valueOf(databaseError));
      }
    });
  }

  public static void removeCommentUpVote(DatabaseReference postRef) {
    postRef.runTransaction(new Transaction.Handler() {

      @Override public Transaction.Result doTransaction(MutableData mutableData) {
        PostDetail n = mutableData.getValue(PostDetail.class);
        if (n == null) {
          return Transaction.success(mutableData);
        } else {
          Integer parsedUpvotes = Integer.parseInt(n.upvotes) - 1;

          n.upvotes = parsedUpvotes.toString();
        }
        mutableData.setValue(n);
        return Transaction.success(mutableData);
      }

      @Override
      public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
        Log.v(TAG, String.valueOf(databaseError));
      }
    });
  }

  public static void checkIfHasUpVoted(DataSnapshot dataSnapshot) {

  }
}
