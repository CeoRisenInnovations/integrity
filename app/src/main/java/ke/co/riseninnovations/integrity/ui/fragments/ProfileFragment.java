package ke.co.riseninnovations.integrity.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.model.Candidate;
import ke.co.riseninnovations.integrity.ui.base.BaseFragment;

public class ProfileFragment extends BaseFragment {
  private static final String TAG = "ProfileFragment";

  @BindView(R.id.tvNumber5)
  TextView mCandidateName;

  ValueEventListener candidateName = new ValueEventListener() {
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
      Candidate candidate = dataSnapshot.getValue(Candidate.class);

      mCandidateName.setText(candidate.firstName);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
      Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
    }
  };

  private DatabaseReference mCandidateRef;

  public ProfileFragment() {
    // need constructor for firebase
  }

  public static ProfileFragment newInstance(String title, DatabaseReference candidateRef) {
    Log.v(TAG, "new instance of");
    return new ProfileFragment();
  }

  @Override public void onAttach(Context context) {
    Log.v(TAG, "onAttach");
    super.onAttach(context);
    try {
      mFragListener = (OnFragmentInteractionListener) context;

    } catch (ClassCastException e) {
      Log.v(TAG, "Exception:" + e);
      throw new ClassCastException(
          context.toString() + " must implement OnFragmentInteractionListener");
    }
    mCandidateListener = (ICandidateProfileListener) context;
  }

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    setRetainInstance(true);
  }

  @Override public void onPrepareOptionsMenu(Menu menu) {
    menu.findItem(R.id.add_new_post).setVisible(false);
    super.onPrepareOptionsMenu(menu);
  }

  @Nullable @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    Log.v(TAG, "onCreateView");
    if (mFragListener != null) {
      //setupTitleBar();
    }
    View view = inflater.inflate(R.layout.fragment_profile, container, false);
    ButterKnife.bind(this, view);

    return view;
  }

  private void setupTitleBar() {
    mFragListener.onFragmentInteraction("Profile");
  }

  public void setReferences(DatabaseReference mCandidateRef) {
    this.mCandidateRef = mCandidateRef;
  }

  @Override public void onStart() {
    mCandidateRef.addListenerForSingleValueEvent(candidateName);
    super.onStart();
  }

  @Override public void onResume() {
    super.onResume();
  }

  @Override public void onPause() {
    super.onPause();
  }

  @Override public void onStop() {
    mFragListener = null;
    super.onStop();
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
  }

  @Override public void onDestroy() {
    super.onDestroy();
  }

  @Override public void onDetach() {
    super.onDetach();
  }

  @Override public String getTitle() {
    return null;
  }
}
