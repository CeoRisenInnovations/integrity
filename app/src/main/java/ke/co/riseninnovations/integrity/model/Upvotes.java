package ke.co.riseninnovations.integrity.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Simon on 12/21/2016.
 */
@IgnoreExtraProperties public class Upvotes {

  public Boolean hasUpvoted;

  public Upvotes() {
    // Default constructor required for calls to DataSnapshot.getValue(Upvotes.class)
  }

  public Upvotes(Boolean hasUpvoted) {
    this.hasUpvoted = hasUpvoted;
  }
}
