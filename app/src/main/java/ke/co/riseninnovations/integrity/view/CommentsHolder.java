package ke.co.riseninnovations.integrity.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.model.PostDetailComments;

/**
 * Created by simon on 9/30/2016.
 */

public class CommentsHolder extends RecyclerView.ViewHolder {
  View mView;
  @BindView(R.id.comment_body) TextView commentView;
  @BindView(R.id.upvote_txt) TextView upvoteView;
  @BindView(R.id.thumbs_up_button) ImageButton upVoteButton;
  @BindView(R.id.time_posted) TextView timeCreatedView;
  @BindView(R.id.username) TextView userNameView;

  public CommentsHolder(View itemView) {
    super(itemView);
    mView = itemView;
    ButterKnife.bind(this, itemView);
  }

  public void setCommentView(String comments) {
    TextView field = (TextView) mView.findViewById(R.id.comment_body);
    field.setText(comments);
  }

  public void setUpvoteView(String upvotes) {
    TextView field = (TextView) mView.findViewById(R.id.upvote_txt);
    field.setText(upvotes);
  }

  public void setTimeCreatedView(String timeCreated) {
    TextView field = (TextView) mView.findViewById(R.id.time_posted);
    field.setText(timeCreated);
  }

  public void setUserNameView(String username) {
    TextView field = (TextView) mView.findViewById(R.id.username);
    field.setText(username);
  }

  public void commentBindToView(PostDetailComments postDetailComments,
      View.OnClickListener commentsOnClickListener) {
    commentView.setOnClickListener(commentsOnClickListener);
  }

  public void upvotesBindToView(PostDetailComments postDetailComments,
      View.OnClickListener upVotesOnClickListener) {
    upvoteView.setOnClickListener(upVotesOnClickListener);
  }

  public void upVoteButtonBindToView(PostDetailComments postDetailComments,
      View.OnClickListener upButtonOnClickListener) {
    upVoteButton.setOnClickListener(upButtonOnClickListener);
  }
}
