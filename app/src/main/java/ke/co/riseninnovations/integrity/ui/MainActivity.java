package ke.co.riseninnovations.integrity.ui;

import static android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import ke.co.riseninnovations.integrity.R;
import ke.co.riseninnovations.integrity.model.User;
import ke.co.riseninnovations.integrity.ui.base.BaseActivity;
import ke.co.riseninnovations.integrity.ui.base.BaseFragment;
import ke.co.riseninnovations.integrity.ui.fragments.CandidatesFragment;
import ke.co.riseninnovations.integrity.ui.fragments.DetailsFragment;
import ke.co.riseninnovations.integrity.ui.fragments.NewPostFragment;
import ke.co.riseninnovations.integrity.ui.fragments.NewsFeedFragment;
import ke.co.riseninnovations.integrity.ui.fragments.ProfileFragment;
import ke.co.riseninnovations.integrity.ui.login.EmailPasswordActivity;

public class MainActivity extends BaseActivity
    implements BaseFragment.IFirebaseReferenceListener, BaseFragment.ICandidateProfileListener,
    BaseFragment.OnFragmentInteractionListener {

  private static final String TAG = "Main Activity";

  // butter knife http://jakewharton.github.io/butterknife/
  @BindView(R.id.toolbar)
  Toolbar toolbar;

  // [START} Define and/or initialize navigation drawer items https://github.com/mikepenz/MaterialDrawer
  PrimaryDrawerItem item1 =
      new PrimaryDrawerItem().withIdentifier(1).withName(R.string.drawer_item_home);
  PrimaryDrawerItem item2 =
      new PrimaryDrawerItem().withIdentifier(2).withName(R.string.drawer_item_candidates);
  FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
  String uid = user.getUid();
  // [END] Fragments

  String profileFrag = "profilefrag";
  private IProfile profile;

  private AccountHeader headerResult = null;
  private Drawer result = null;

  // [START] Define google.firebase objects https://firebase.google.com/
  private DatabaseReference database = FirebaseDatabase.getInstance().getReference();
  private FirebaseAuth.AuthStateListener mAuthListener;
  private FirebaseAuth mAuth;
  // [END] Define google.firebase objects

  // [START] Fragments
  private NewsFeedFragment newsFeedFragment;
  private NewPostFragment newPostFragment;
  private DetailsFragment detailsFragment;
  private CandidatesFragment candidatesFragment;
  private ProfileFragment profileFragment;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Log.v(TAG, "onCreate: ");

    setupFirebase();

    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    setSupportActionBar(toolbar);

    ValueEventListener usernameListener = new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        User mUser = dataSnapshot.getValue(User.class);
        String username = mUser.username;
        String email = mUser.email;
        if (profile == null) {
          profile = new ProfileDrawerItem().withName(username)
              .withEmail(email)
              .withIcon(R.drawable.ic_account_circle_black_24dp);
          // Create the AccountHeader
          buildHeader(false, savedInstanceState);
          initDrawer(savedInstanceState);
        }
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {
        Log.v(TAG, "Network connection unavailable...");
      }
    };

    database.child("users/" + uid).addListenerForSingleValueEvent(usernameListener);

    if (savedInstanceState == null) {
      if (newsFeedFragment == null) {
        newsFeedFragment = NewsFeedFragment.newInstance("newsFeedFrag");
      }
      ft.add(R.id.newsfeedContainer, newsFeedFragment, "newsfeed");
      ft.commit();
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    Log.v(TAG, "onStart: ");
    mAuth.addAuthStateListener(mAuthListener);
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.v(TAG, "onResume: ");

    initFragments();
  }

  @Override
  protected void onPause() {
    super.onPause();
    Log.v(TAG, "onPause: ");
  }

  @Override
  public void onStop() {
    super.onStop();
    Log.v(TAG, "onStop: ");
    if (mAuthListener != null) {
      mAuth.removeAuthStateListener(mAuthListener);
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    Log.v(TAG, "onDestroy: ");
  }

  @Override
  public Object onRetainCustomNonConfigurationInstance() {
    return super.onRetainCustomNonConfigurationInstance();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
      case R.id.sign_out_menu:
        signOut();
        return true;
      case R.id.add_new_post:
        openNewPostFrag();
        break;
      default:
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    Log.v(TAG, "Create Options Menu");
    return true;
  }

  @Override
  public void iFirebaseReferenceListener(DatabaseReference globalPostRef,
      DatabaseReference globalUserPostRef, String newsfeedKey) {
    Log.v(TAG, "IFirebaseReferenceListener");
    detailsFragment =
        DetailsFragment.newInstance(null, globalPostRef, globalUserPostRef, newsfeedKey);
    detailsFragment.setGlobalReferences(globalPostRef, globalUserPostRef, newsfeedKey);
    if (getResources().getBoolean(R.bool.twoPaneMode)) {
      // Screen is large enough to support two fragments
      Log.v(TAG, "Two panel mode");
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.newsfeedContainer, detailsFragment)
          .addToBackStack("details")
          .commit();
    } else {
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.newsfeedContainer, detailsFragment)
          .addToBackStack("details")
          .commit();
    }
  }

  @Override
  public void iCandidateProfileListener(DatabaseReference candidateRef) {
    profileFragment = ProfileFragment.newInstance(null, candidateRef);
    profileFragment.setReferences(candidateRef);
  }

  @Override
  public void onFragmentInteraction(final String title) {
    fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
      @Override
      public void onBackStackChanged() {
        // Update your UI here.
        if (getSupportActionBar() != null) {
          getSupportActionBar().setTitle(title);
        }
      }
    });
  }

  @Override
  public void onBackPressed() {
    int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
    FragmentManager fm = getSupportFragmentManager();
    Fragment currentFragment = fm.findFragmentById(R.id.newsfeedContainer);

    Log.v(BASETAG, "Items in backstack: " + backStackCount + "Current Frag: " + currentFragment);
    AlertDialog.Builder builder = new AlertDialog.Builder(this);

    if (backStackCount == 0) {
      showExitBuilderDialog(builder);
    } else if (currentFragment instanceof NewsFeedFragment) {
      builder.setMessage("Are you sure you want to exit?")
          .setCancelable(false)
          .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              MainActivity.this.finish();
            }
          })
          .setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              dialog.cancel();
            }
          });
      AlertDialog alert = builder.create();
      alert.show();
    } else if (currentFragment instanceof NewPostFragment) {
      newPostFragment.mFragListener = null;
      getSupportFragmentManager().popBackStack();
    } else if (currentFragment instanceof CandidatesFragment) {
      candidatesFragment.mFragListener = null;
      getSupportFragmentManager().popBackStack();
    } else if (currentFragment instanceof DetailsFragment) {
      detailsFragment.mFragListener = null;
      getSupportFragmentManager().popBackStack();
      super.onBackPressed();
    } else if (currentFragment instanceof ProfileFragment) {
      profileFragment.mFragListener = null;
      getSupportFragmentManager().popBackStack();
    } else {
      super.onBackPressed();
    }
  }

  private void showExitBuilderDialog(AlertDialog.Builder builder) {
    builder.setMessage("Are you sure you want to exit?")
        .setCancelable(false)
        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            finish();
          }
        })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
          }
        });
    AlertDialog alert = builder.create();
    alert.show();
  }

  //sign out method
  public void signOut() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage("Are you sure you want to sign out?")
        .setCancelable(false)
        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            mAuth.signOut();
            finish();
            startActivity(new Intent(MainActivity.this, EmailPasswordActivity.class));
          }
        })
        .setNegativeButton("No", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
          }
        });
    AlertDialog alert = builder.create();
    alert.show();

    Log.v(TAG, "Sign Out");
  }

  private void initDrawer(Bundle savedInstanceState) {
    result = new DrawerBuilder().withActivity(MainActivity.this)
        .withToolbar(toolbar)
        .withAccountHeader(headerResult)
        .addDrawerItems(item1, item2, new DividerDrawerItem())
        .withDrawerLayout(R.layout.material_drawer_fits_not)
        .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
          @Override
          public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (position == 1) {
              naviHomePressed(transaction);
            } else if (position == 2) {
              naviCandidatesPressed(transaction);
            }
            return true;
          }
        })
        .withSavedInstance(savedInstanceState)
        .build();
  }

  private void setupFirebase() {
    // [START] Get firebase instances
    mAuth = FirebaseAuth.getInstance();
    // [END] Get firebase instances

    // [START] Authentication
    mAuthListener = new FirebaseAuth.AuthStateListener() {
      @Override
      public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        if (user == null) {
          // user auth state is changed - user is null
          // launch login activity
          Log.v(TAG, "User is signed out");
          startActivity(new Intent(MainActivity.this, EmailPasswordActivity.class));
          finish();
        }
      }
    };
    // [END] Authentication
  }

  private void buildHeader(boolean compact, Bundle savedInstanceState) {
    // Create the AccountHeader
    headerResult = new AccountHeaderBuilder().withActivity(this)
        .withHeaderBackground(R.drawable.header)
        .withCompactStyle(compact)
        .addProfiles(profile)
        .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
          @Override
          public boolean onProfileChanged(View view, IProfile profile, boolean current) {

            //false if you have not consumed the event and it should close the drawer
            return false;
          }
        })
        .withSavedInstance(savedInstanceState)
        .build();
  }

  private void naviHomePressed(FragmentTransaction transaction) {
    Log.v(TAG, "Home Navi Clicked");

    Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.newsfeedContainer);
    if (currentFrag instanceof NewsFeedFragment) {
      result.closeDrawer();
    } else {
      if (newsFeedFragment == null) {
        Log.v(TAG, "newsFeedFragment is null");

        newsFeedFragment = NewsFeedFragment.newInstance("newsFeedFrag");

        getSupportFragmentManager().popBackStack(null, POP_BACK_STACK_INCLUSIVE);
        transaction.replace(R.id.newsfeedContainer, newsFeedFragment);
        transaction.addToBackStack("newsfeed");
        transaction.commit();
      } else {
        getSupportFragmentManager().popBackStack(null, POP_BACK_STACK_INCLUSIVE);
        transaction.replace(R.id.newsfeedContainer, newsFeedFragment);
        transaction.addToBackStack("newsfeed");
        transaction.commit();
      }
    }

    result.closeDrawer();
  }

  private void naviCandidatesPressed(FragmentTransaction transaction) {
    Log.v(TAG, "Candidates Navi Clicked");

    Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.newsfeedContainer);
    if (currentFrag instanceof CandidatesFragment) {
      Log.v(TAG, "Current Fragment is the Candidates");
      // Will result in the drawer closing
    } else {
      if (candidatesFragment != null) {
        transaction.replace(R.id.newsfeedContainer, candidatesFragment);
        transaction.addToBackStack("candidates");
        transaction.commit();
      } else {
        candidatesFragment = CandidatesFragment.newInstance("candidatesFrag");
        naviCandidatesPressed(transaction);
      }
    }
    result.closeDrawer();
  }

  private void initFragments() {
    if (newsFeedFragment == null) {
      newsFeedFragment = NewsFeedFragment.newInstance("newsFeedFrag");
    }

    if (newPostFragment == null) {
      newPostFragment = NewPostFragment.newInstance("newPostFrag");
    }

    if (detailsFragment == null) {
      detailsFragment = DetailsFragment.newInstance("detailsFrag", null, null, null);
    }

    if (candidatesFragment == null) {
      candidatesFragment = CandidatesFragment.newInstance("candidatesFrag");
    }

    if (profileFragment == null) {
      profileFragment = ProfileFragment.newInstance("profileFrag", null);
    }
  }

  public void openNewPostFrag() {
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    if (!newPostFragment.isVisible()) {
      ft.replace(R.id.newsfeedContainer, newPostFragment);

      // Commit changes
      ft.addToBackStack("newpost");
      ft.commit();
    }
  }

  public void openGovFrag() {
    Log.v(TAG, "Launch Gov Frag");
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.newsfeedContainer, profileFragment)
        .addToBackStack(profileFrag)
        .commit();
    candidatesFragment.mFragListener.onFragmentInteraction("Governor");
    //profileFragment.setReferences(governorRef);
  }

  public void openSenatorFrag() {
    final DatabaseReference senatorRef = database.child("candidates/nairobi").child("senator");
    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
    ft.replace(R.id.newsfeedContainer, profileFragment);
    ft.addToBackStack("profilefrag");
    ft.commit();

    candidatesFragment.setFragmentListener("Senator");

    profileFragment.setReferences(senatorRef);
  }

  /*public void openMPFrag() {
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.newsfeedContainer, profileFragment)
        .addToBackStack(profileFrag)
        .commit();
    candidatesFragment.mFragListener.onFragmentInteraction("MP");
  }

  public void openMCAFrag() {
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.newsfeedContainer, profileFragment)
        .addToBackStack(profileFrag)
        .commit();
    candidatesFragment.mFragListener.onFragmentInteraction("MCAs");
  }*/

  public boolean isNetworkConnected() {
    ConnectivityManager connMgr = (ConnectivityManager)
        getSystemService(Context.CONNECTIVITY_SERVICE); // 1
    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo(); // 2
    return networkInfo != null && networkInfo.isConnected(); // 3
  }

  public void setTitle(String title) {
    if (getSupportActionBar() != null) {
      getSupportActionBar().setTitle(title);
    }
  }

  public int getScreenOrientation() {
    return this.getResources().getConfiguration().orientation;
  }
}
